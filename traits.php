<?php

trait trait1
{
    public static function call1()
    {
        echo 'trait1';
    }
}

trait trait2
{
    public function call2()
    {
        echo 'trait2';
    }
}

class People
{
    use trait1;
    use trait2;
}

// $people = new People();
// $people::call1();
// $people->call2();

People::call1();
