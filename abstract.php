<?php

// abstract class Animals
// {
//     public abstract function eat();
//     public abstract function sleep();
//     public function isAnimals()
//     {
//         echo 'true';
//     }
// }

// class Cat extends Animals
// {
//     public function eat()
//     {
//         echo "eat";
//     }

//     public function sleep()
//     {
//         echo "sleep";
//     }
// }

// $cat = new Cat();
// $cat->eat();
// $cat->sleep();
// $cat->isAnimals();

abstract class Receipe
{
    protected function addSalt()
    {
        var_dump('add salt');
        return $this;
    }

    protected function addPaper()
    {
        var_dump('add Paper');
        return $this;
    }

    public function toCook()
    {
        return $this
            ->addMainIngredient()
            ->addSalt()
            ->addPaper();
    }

    protected abstract function addMainIngredient();
}

class ChickenCurry extends Receipe
{
    public function addMainIngredient()
    {
        var_dump('add chicken');

        return $this;
    }
}

// $chickenCurry = new ChickenCurry();

// $chickenCurry->toCook();

class PorkCurry extends Receipe
{
    public function addMainIngredient()
    {
        var_dump('add pork');

        return $this;
    }
}

$porkCurry = new PorkCurry();
$porkCurry->toCook();
