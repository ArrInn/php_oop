<?php

class Food
{
    protected $food;

    public function __construct($food)
    {
        $this->food = $food;
    }

    public function get()
    {
        return $this->food;
    }
}

// class Dinner
// {
//     public function eat()
//     {
//         $food = new Food('Pork Curry in Dinner class');
//         return $food->get();
//     }
// }

// $dinner = new Dinner();
// echo $dinner->eat();

class Dinner
{
    public static function eat(Food $food)
    {
        return $food->get();
    }
}

$food = new Food('Pork Curry in DI');

echo Dinner::eat($food);
