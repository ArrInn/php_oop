<?php
// class Country
// {
//     public static $name = "Myanmar";

//     public function getName()
//     {
//         echo self::$name;
//     }
// }

// $country = new Country();
// $country->getName();


// class Country
// {
//     public static $name = "Myanmar";
// }

// class Myanmar extends Country
// {
//     public function getName()
//     {
//         echo parent::$name . 'child class';
//     }
// }

// $country = new Myanmar();
// $country->getName();

class Country
{
    public static $name = "Myanmar";
}

class Myanmar extends Country
{
    public static function getName()
    {
        echo parent::$name . 'child static method';
    }
}

Myanmar::getName();
