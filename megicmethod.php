<?php

// class Person
// {
// public function __construct()
// {
//     $this->name = "MGMG";
// }

// public function __set($name, $value)
// {
//     $this->name = $value;
// }
// }

// $people = new Person;

// echo $people->name;


// $person = new Person;
// echo $person->value = "AG AG";

// class HtmlElement
// {
//     private $attributes = [];

//     private $tag;

//     public function __construct($tag)
//     {
//         $this->tag = $tag;
//     }

//     public function __set($name, $value)
//     {
//         $this->attributes[$name] = $value;
//     }

//     public function html($innerHTML = '')
//     {
//         $html = "<{$this->tag}";
//         foreach ($this->attributes as $key => $value) {
//             $html .= ' ' . $key . '="' . $value . '"';
//         }
//         $html .= '>';
//         $html .= $innerHTML;
//         $html .= "</$this->tag>";

//         return $html;
//     }
// }


// class Test extends HtmlElement
// {
// }

// $div = new HtmlElement('div');
// $div = new Test('div');

// $div->id = 'page';
// $div->class = 'light';

// echo $div->html('Hello');


// class MethodTest
// {
//     public function __call($name, $arguments)
//     {
//         // Note: value of $name is case sensitive.
//         echo "Calling object method '$name' "
//             . implode(', ', $arguments) . "\n";
//     }

//     public static function __callStatic($name, $arguments)
//     {
//         // Note: value of $name is case sensitive.
//         echo "Calling static method '$name' "
//             . implode(', ', $arguments) . "\n";
//     }
// }

// $obj = new MethodTest;
// $obj->runTest('in object context', 'hello world');

// MethodTest::test('in static context');
