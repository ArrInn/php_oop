<?php

// class Person
// {
//     public $name;
//     public $age;

//     public function __construct($name, $age)
//     {
//         $this->name = $name;
//         $this->age = $age;
//     }

//     public function getName()
//     {
//         return $this->name;
//     }

//     public function getAge()
//     {
//         return $this->age;
//     }
// }


// class MgMg extends Person
// {
// }

// $mgmg = new MgMg('Mg Mg', 30);
// echo 'name -> ' . $mgmg->name . ' age -> ' . $mgmg->age;

// $mgmg = new MgMg('Ag Ag', 26);
// echo 'name -> ' . $mgmg->name . ' age -> ' . $mgmg->age;

class Person
{
    public $name;
    public $age;

    public function __construct($name, $age)
    {
        $this->name = $name;
        $this->age = $age;
    }

    public function getName()
    {
        return $this->name;
    }

    protected function getAge()
    {
        return $this->age;
    }
}


class MgMg extends Person
{
    public function output()
    {
        echo $this->getAge();
    }
}

$mgmg = new MgMg('Mg Mg', 40);
echo 'name -> ' . $mgmg->name;
echo '----------';
$mgmg->output();

// $mgmg = new MgMg('Ag Ag', 26);
// echo 'name -> ' . $mgmg->name . ' age -> ' . $mgmg->age;
