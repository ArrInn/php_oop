<?php

interface Animals
{
    public function eat();
    public function sleep();
}

class Cat implements Animals
{
    public function eat()
    {
        echo "eat";
    }

    public function sleep()
    {
        echo "sleep";
    }
}

$cat = new Cat();
$cat->eat();
$cat->sleep();
